<?php
if(isset($_GET['nome'])){
    $nomerec = $_GET['nome'];
    $emailrec = $_GET['email'];
    $anorec = $_GET['ano'];
    
    $n_letras_nome = conta_letras($nomerec);
    $n_letras_email = conta_letras($emailrec);
}
function conta_letras($value=''){
    $value = trim($value);
    return strlen($value);
}
function retorna_dominio($email=''){
    $pos_arroba = 0;
    $n_letras = strlen($email);
    for($i = 0;$i < $n_letras;$i++){
        if(substr($email, $i, 1)=='@'){
            $pos_arroba = $i;
        }
    }
    if (!$pos_arroba==0){
        $dominio = substr($email, $pos_arroba + 1);
        return $dominio;
    }
}

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulário de contato - Aulas UC12 - PHP</title>
</head>
<body>

    <h1>Formulário de contato</h1>
    <br>
    <form action="#" method="get">
        <label for="nome">Nome</label>
        <input type="text" name="nome" value="" size="45px" placeholder="Digite o nome..."
        required="vai maluco" >
        <br>
        <br>
        <label for="email">Email</label>
        <input type="email" name="email" size="45px" required placeholder="Digite o email">
        <br>
        <br>
        <label for="ano">Ano</label>
        <input type="text" name="ano" size="20px" required placeholder="digite o ano letivo">
        <br>
        <br>
        <input type="submit" value="Gravar" name="gravar">
    </form>
    <br>
    <hr>
    <br>
    <?php
        if (isset($nomerec)){
            echo "Dados retornados...<br>
            Nome: $nomerec <br>
            Email: $emailrec <br>
            Ano: $anorec <br>
            Seu nome possui:  $n_letras_nome letras <br>
            Seu email possui:  $n_letras_email letras <br>";
            echo "O domínio do email é: ".retorna_dominio($emailrec);
        }
    ?>
    <table border='1px'>
        <?php
            $n = strlen($emailrec);
            for ($i = 0; $i < $n ; $i++){
                $m  = substr($emailrec, $i, 1);
                echo "<tr id='$i'><td>$m</td></tr>";
            }
        ?>
    </table>


</body>
</html>